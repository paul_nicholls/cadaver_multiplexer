
ACME=tools\acme.exe 

DEPS = 	main.mfk \
				modules\multiplex.mfk \
				bin\multiplex.bin 

all: bin\main.prg

bin\multiplex.bin: asm\multiplexer.s 
	$(ACME) asm\multiplexer.s  	

bin\main.prg:	$(DEPS)
	millfork -G vice main.mfk -i modules -i . -t c64-plex -o bin\main.prg
