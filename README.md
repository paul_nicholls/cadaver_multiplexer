URLs:
// Millfork source
https://github.com/KarolS/millfork

// Awesome human who wraped Cadaver's multiplexer code for using with Millfork!!
https://twitter.com/MonstersGo

Here you will find a Millfork wrapped version of Cadaver's C64 sprite multiplexer code (maximum 24 sprites).
@monstersgo on Twitter kindly wrapped the Cavaver code up in Millfork

* file information:
-------------------
asm multiplexer code, yay!
asm\multiplexer.s

the folder where the .prg file is compiled to (can change in make/batch files)

bin\

Millfork wrapper for the assembler code (Whoot!)

modules\multiplexer.mfk

the included assembler that compiles the multiplexer code into the .bin version for inclusion into the main.mfk code!

tools\ACME.exe

the make file (if you use such things)

makefile

The windows batch file for compiling and running the main.mfk code

compileAndRun.bat

How to use the code
-------------------------------------------------

SpriteXlsb[MAX_SPRITES]  = sprite x lsb location
SpriteXmsb[MAX_SPRITES]  = sprite x msb location
SpriteY[MAX_SPRITES]     = sprite y location

SpriteColor[MAX_SPRITES] = sprite color (0..15)
SpriteFrame[MAX_SPRITES] = sprite frame pointer index in current VIC bank

// the good stuff, ie. cody bits :D

void main{
// initialization code
	asm {sei}

  // special multiplexer ram conditions
	c64_ram_io()

	// init multiplexer
	MPlex_Init()
	MPlex_SetScreen($04)	//	msb of screen $0400 for testing


	asm {cli}

  byte i;

  // main loop

  while true {

		MPlex_Update()

    for i,0,to,MAX_SPRITES - 1 {
      // update sprite X,Y, color, and/or frames as necessary
      SpriteXlsb[i]  = ?
      SpriteXmsb[i]  = ?
      SpriteY[i]     = ?

      SpriteColor[i] = ?
      SpriteFrame[i] = ?
    }

    // rinse and repeat :D
  }
}

NOTE
-------------------------------------------
To change the multiplexer code location:
change $3800 to where you need to, both MUST match :)

modules\multiplex.mfk

const word MplexBase = $3800 <<

asm\multiplexer.s
			; Main program
			* = $3800	<<
-------------------------------------------
